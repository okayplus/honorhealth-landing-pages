var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    nunjucks = require('gulp-nunjucks'),
    livereload = require('gulp-livereload');

gulp.task('sass', function () {
    var options = {
        sourceMap: true,
        outputStyle: 'compressed',
        includePaths: [
            'node_modules'
        ]
    },
    plugins = [
        autoprefixer({browsers: [
            'Chrome >= 35',
            'Firefox >= 38',
            'Edge >= 12',
            'Explorer >= 10',
            'iOS >= 8',
            'Safari >= 8',
            'Android 2.3',
            'Android >= 4',
            'Opera >= 12'
        ]})
    ];

    return gulp.src('./sass/**/*.scss')
        .pipe(plumber(function (error) {
            console.error(error.message);
            this.emit('end');
        }))
        .pipe(sass(options))
        .pipe(postcss(plugins))
        .pipe(gulp.dest('./public/css'))
        .pipe(livereload());
});

gulp.task('html', function() {
    return gulp.src('./html/**/[^_]*.html')
        .pipe(plumber(function (error) {
            console.error(error.message);
            this.emit('end');
        }))
        .pipe(nunjucks.compile())
        .pipe(gulp.dest('./public'))
        .pipe(livereload());
});

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('./sass/**/*.scss', ['sass']);
    gulp.watch('./html/**/*.html', ['html']);
});

gulp.task('default', ['sass', 'html', 'watch']);
